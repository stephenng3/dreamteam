package rep.dreamteam;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextView results;
    int latestResult = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        results = ((TextView) findViewById(R.id.result));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Right now, rolling the dice returns a purely random result.
                // How do I, as an immoral CNY gambling den host, rig the result to only stop when 3 is rolled?
                rollDice();
            }
        });

        findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results.setText("");
                latestResult = 0;
            }
        });
    }

    private void rollDice(){
        latestResult = (int)(Math.random()*6+1);
        results.setText(results.getText()+"\n"+"Rolled: "+String.valueOf(latestResult));
    }
}
